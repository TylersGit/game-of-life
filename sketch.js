// An implementation of Connor's game of Life in p5.js

const scale = 10;
const [width, height] = [1080, 720];
console.log(width, height);

let grid;
let button;

function setup() {
  // No loop, for testing purposes.
  // noLoop();
  createCanvas(width + 1, height + 1);
  background(0);
  fill(255);
  frameRate(15);

  grid = new Grid(width / scale, height / scale);

  grid.show();

  // button = createButton("Next generation");
  // button.mousePressed(() => grid.updateGrid());
}

function draw() {
  grid.updateGrid();
}
