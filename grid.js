class Grid {
  constructor(rows, columns) {
    this.rows = rows;
    this.columns = columns;
    this.grid = this.createGrid(this.rows, this.columns);
  }

  createGrid = (rows, columns) => {
    let grid = new Array(rows);
    for (let i = 0; i < rows; i++) {
      grid[i] = new Array(columns);
    }

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < columns; j++) {
        // Initialize each cell to be alive or dead randomly.
        grid[i][j] = new Cell(i, j);
      }
    }

    return grid;
  };

  show() {
    this.grid.forEach((innerArray) => {
      innerArray.forEach((cell) => {
        cell.show();
      });
    });
  }

  getNeighbors() {
    this.grid.forEach((innerArray) => {
      innerArray.forEach((cell) => {
        // console.log(1);
        cell.getNeighbors();
      });
    });
  }

  checkForLivingNeighbors() {
    this.grid.forEach((innerArray) => {
      innerArray.forEach((cell) => {
        // console.log(2);
        cell.checkForLivingNeighbors();
      });
    });
  }

  updateStates() {
    // console.log(3);
    this.grid.forEach((innerArray) => {
      innerArray.forEach((cell) => {
        cell.updateState();
      });
    });
  }

  updateGrid() {
    this.getNeighbors();
    this.checkForLivingNeighbors();
    this.updateStates();
    this.show();
  }
}
