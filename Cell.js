const chanceToStartAlive = 10;

class Cell {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.alive = random(1) < chanceToStartAlive / 100 ? true : false;
    this.neighbors = [];
    this.livingNeighbors = 0;
  }

  show() {
    if (!this.alive) {
      fill(255);
    } else {
      fill(0);
    }
    rect(this.x * scale, this.y * scale, scale - 1, scale - 1);
  }

  getNeighbors() {
    let startX = this.x - 1;
    let endX = this.x + 2;
    let startY = this.y - 1;
    let endY = this.y + 2;

    if (this.x === 0) {
      startX = 0;
      endX = 2;
    }
    if (this.x === width / scale) {
      startX = width / scale - 1;
      endX = width / scale + 1;
    }
    if (this.y === 0) {
      startY = 0;
    }
    if (this.y === height / scale) {
      startY = height / scale - 1;
      endY = height / scale + 1;
    }

    // Outer array is rows - Y values
    // Inner array is columns - X values
    let rowsSlice = grid.grid.slice(startX, endX);
    let columnsSlice = rowsSlice.map((innerArray) => {
      return innerArray.slice(startY, endY);
    });

    this.neighbors = columnsSlice.map((arr) =>
      arr.filter((cell) => {
        // Again, Y and X are confusing.
        // Refactoring is in order.
        if (cell.x == this.x && cell.y == this.y) {
          // Returning null achieves the result
          // of removing this element from the array.
          return null;
        }
        return cell;
      })
    );
  }

  checkForLivingNeighbors() {
    // This is a helper function for updateState.
    // It counts the number of living neighbors.
    let livingNeighbors = 0;

    this.neighbors.map((innerArray) => {
      innerArray.forEach((cell) => {
        if (cell.alive) {
          livingNeighbors++;
        }
      });
    });

    this.livingNeighbors = livingNeighbors;
  }

  updateState() {
    if (this.alive) {
      if (this.livingNeighbors === 2 || this.livingNeighbors === 3) {
        this.alive = true;
      } else {
        this.alive = false;
      }
    } else if (!this.alive) {
      if (this.livingNeighbors === 3) {
        this.alive = true;
      }
    }
  }
}
