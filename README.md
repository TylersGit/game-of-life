# Conway's Game of Life

This is a simple implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life), made using the [p5.js](https://p5js.org/) library.

This was made as a fun project to learn how to use the p5.js library, and to keep my programming skills up to sharp.

Currently, only the simulation is implemented.
In the future, I plan to add a GUI to control the simulation, as well as the ability to determine the starting state of the simulation.

## How to use

To view the simulation, open the index.html file in your browser.  
In order to restart the simulation, refresh the page.
